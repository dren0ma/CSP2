<?php 
function display_title(){
	echo "BagPack | Favorites";
}

function display_content() { 
require 'dbconnect.php';

if (isset($_SESSION['username'])){
	?>
	<div class="section">
		<p class="title has-text-centered">
			Favorites
		</p>
	</div>
	<?php
	//get account Id
	$username = $_SESSION['username'];
	$sql = "SELECT acctId FROM accounts WHERE username = '$username'";
	$result = mysqli_query($dbcon, $sql);
	$dbarray = mysqli_fetch_assoc($result);
	$acctId = $dbarray['acctId'];

	//favorites iteration
	
	$sql = "SELECT name, img, price, brand, items.itemId, acctId
		FROM favorites
		JOIN items ON favorites.itemId = items.itemId
		JOIN brands ON items.brandId = brands.brandId
		WHERE acctId = '$acctId'";
	$result = mysqli_query($dbcon, $sql);
	while ($dbarray = mysqli_fetch_assoc($result)) {
		extract($dbarray);
		?>
		<div class="container" <?php echo "id='deleteFav".$itemId."'>";?>
			<div class="section">
				<div class="columns">
					<!-- item img -->
					<div class="column is-one-fourth is-offset-2">
						<figure class="image is-24x24 delete-item">
							<img src="assets/img/icons/exit.png" class="del-fav" <?php echo "data-id='$itemId'>" ?>
						</figure>
						<figure class="image is-128x128">
							<img src="<?php echo $img; ?>">
						</figure>
					</div>
					<!-- item name, brand -->
					<div class="column is-one-fourth	">
						<p class="title is-5 font-color">
							<?php echo $brand; ?>
						</p>
						<p class="subtitle">
							<?php echo $name; ?>
						</p>
						<p class="title is-5 price-color">
							<?php echo "₱ ".$price; ?>
						</p>
					<!-- view/cart -->
						<?php echo "<a href='viewitem.php?itemId=$itemId'>"; ?>
							<input class="button is-info" type="button" value="View Item">
						</a>
						<input class="button is-dark add-cart-fav" type="button" <?php echo "data-id='$itemId' data-qty='1'" ?> value="Add to Cart">
					</div>
				</div>
			</div>
		</div>	<!-- /container -->
	<?php }	//while 

}	//if
else {
	?>
	<section class="hero is-medium">
		<div class="hero-body">
			<div class="container">
				<h1 class="title">
					You are not logged in.
				</h1>
				<h2 class="subtitle">
					Please <a href="login.php">log in </a>to view your Favorites 
				</h2>
			</div>
		</div>
	</section>
	<?php
}	//else


}	//function display
require "partials/main.php";

?>