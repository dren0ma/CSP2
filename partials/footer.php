<footer class="footer">
	<div class="columns">
		<div class="column is-3 is-offset-1">
			<a href="index.php">
			<figure class="image">
				<img src="assets/img/logo.png">
			</figure>			
			</a>
		</div>
		<div class="column is-1"></div>
		<div class="column is-2">
			<p class="title is-6">
				Navigation
			</p>
			<p>
				<a href="index.php">
					Home
				</a>
			</p>
			<p>
				<a href="shop.php">
					Collection
				</a>
			</p>
			<p>
				<a href="contact.php">
					Contact Us
				</a>
			</p>
			<p>
				<a href="disclaimer.php">
					Disclaimer
				</a>
			</p>
		</div>
		<div class="column is-2">
			<p class="title is-6">
				Shop
			</p>
			<?php echo "<a href='sort.php?sort=bp'" ?> class="link">
				<p>
					Backpack
				</p>
			</a>
			<?php echo "<a href='sort.php?sort=lap'" ?>
				<p>
					Laptop
				</p>
			</a>
			<?php echo "<a href='sort.php?sort=msgr'" ?>
				<p>
					Messenger
				</p>
			</a>
			<?php echo "<a href='sort.php?sort=sports'" ?>
				<p>
					Sports
				</p>
			</a>
			<?php echo "<a href='sort.php?sort=out'" ?>
				<p>
					Outdoor
				</p>
			</a>
		</div>
		<div class="column is-2">
			<p class="title is-6 has-text-centered">
				Visit us at social media!
			</p>
			<div class="level">

				<figure class="image is-48x48">
					<a href="#"><img src="assets/img/icons/facebook.png"></a>
				</figure>
				<figure class="image is-48x48">
					<a href="#"><img src="assets/img/icons/instagram.png"></a>
				</figure>
				<figure class="image is-48x48">
					<a href="#"><img src="assets/img/icons/twitter.png"></a>
				</figure>
				<figure class="image is-48x48">
					<a href="#"><img src="assets/img/icons/pinterest.png"></a>
				</figure>
			</div>
		</div>
	</div>	

</footer>