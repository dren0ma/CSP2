<?php 
function display_title(){
	echo "Cart";
}
function display_content() { 
require 'dbconnect.php';
if (((isset($_SESSION['cart'])) && (count($_SESSION['cart']) == 0)) || (!isset($_SESSION['cart']))){
	?>
		<section class="hero is-medium">
			<div class="hero-body">
				<div class="container">
					<h1 class="title">
						Your Cart is Empty
					</h1>
					<h2 class="subtitle">
						View our <a href="shop.php">Collection</a>
					</h2>
				</div>
			</div>
		</section>
		<?php
}
 
else if (isset($_SESSION['cart'])){
	?>
		<div class="section">
			<p class="title headers has-text-centered">
				Cart
			</p>
		</div>
	<?php
	//item iteration
	foreach ($_SESSION['cart'] as $id => $quantity) {
		$sql = "SELECT name, img, price, brand, itemId
				FROM items 
				JOIN brands ON items.brandId = brands.brandId
				WHERE itemId = '$id'";
		$result = mysqli_query($dbcon, $sql);
		$dbarray = mysqli_fetch_assoc($result);
		extract($dbarray);		?>
			<div class="container" <?php echo "id='deleteCart".$id."'>";?>
				<div class="section cart-section">
					<div class="columns">
						<!-- item img -->
						<div class="column is-one-fifth is-offset-2">
							<figure class="image is-24x24 delete-item">
								<img src="assets/img/icons/exit.png" class="del-cart" <?php echo "data-id='$id'>" ?>
							</figure>
							<figure class="image is-128x128">
								<img src="<?php echo $img; ?>">
							</figure>
						</div>
						<!-- item name, brand -->
						<div class="column is-one-fifth">
							<p class="title is-5">
								<?php echo $brand; ?>
							</p>
							<p class="subtitle">
								<?php echo $name; ?>
							</p>
						</div>
						<!-- item qty -->
						<div class="column is-one-fifth">
							<nav class="level">
								<div class="level-item">
									<a class="button btnLeft button-border-remove" type="button" data-id="<?php echo $id; ?>" >
									<span class="icon">
										<i class="fas fa-angle-left"></i>
									</span>
									</a>
								</div>
								<div class="level-item">
									<label class="label" data-id='<?php echo "$id'>
										<span id='qtyCart".$id."'>$quantity</span>"?>
									</label>
								</div>
								<div class="level-item">
									<a class="button btnRight button-border-remove" type="button" data-id="<?php echo $id; ?>">
									<span class="icon">
										<i class="fas fa-angle-right"></i>
									</span>
									</a>
								</div>
							</nav>
						</div>
						<!-- item total -->
						<div class="column is-one-fifth">
							<?php echo "<span class='title is-6 has-text-centered price-color' id='itemPrice".$id."' data-price='$price'>";
								echo ("₱ ".$price * $quantity); ?>
							</span>
						</div>
					</div>
				</div>
			</div>	<!-- /container -->
	<?php }	// foreach ?>

	<div class="section">
		<nav class="level">
			<div class="level-item">
				<a href="checkout.php" class="button is-info checkout" type="button">
					Checkout
				</a>
			</div>
		</nav>
	</div>
<?php
}
// if
	
	 
}	//function display
require "partials/main.php";
?>
